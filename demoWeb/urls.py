from django.urls import re_path, include
from . import views
from . import lookups

urlpatterns = [
# Bmisc
    re_path(r'^bMisc/', include('bMisc.urls')),

# start
    re_path(r'^$', views.Start.as_view(), name='start'),
]