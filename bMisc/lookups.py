from dal import autocomplete

from . import models

class lookupReportType(autocomplete.Select2ListView):
    rptList = {'pdf': 'Pdf', 'txt': 'Text File', 'xlsx': 'Excel 2007', 'xlsx-d': 'Excel Data', 'docx': 'Word 2007'}
    def get_list(self):
        print(self.forwarded)
        if self.forwarded:
            return ['pdf', 'txt']

        return ['pdf', 'txt', 'xlsx', 'xlsx-d', 'docx']


class LookupMainDishes(autocomplete.Select2ListView):
    def get_list(self):
        menu = ['BUTTER CHICKEN', 'PALAK PANEER', 'SPICY PORK VINDALOO', 'INDIAN STYLE LENTILS']
        print(self.forwarded)
        return menu


class LookupDessertDishes(autocomplete.Select2ListView):
    def get_list(self):
        menu = ['Mixed berry mousse', 'Mango and coconut soufflé', 'Homemade carrot cake', 'Matcha cake']
        return menu


class LookupMenuList(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        model = models.menu_list
        if not self.request.user.is_authenticated:
            return model.objects.none()

        qs = model.objects.none()

        print(self.forwarded)
        if self.forwarded:
            qs = model.objects.all()

        return qs

