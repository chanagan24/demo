from django.views import generic

from . import forms

# Create your views here.
class Main(generic.FormView):
    template_name = 'main.html'
    form_class = forms.reportFindForm
    # prefix = 'reportRequest'