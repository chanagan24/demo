from django.urls import re_path
from . import views
from . import lookups

app_name = 'bMisc'

urlpatterns = [
# Lookup
    re_path(r'^lookup_report_type/$', lookups.lookupReportType.as_view(), name='lookup_report_type'),
    re_path(r'^lookup_mainDishes/$', lookups.LookupMainDishes.as_view(), name='lookup_mainDishes'),
    re_path(r'^lookup_dessertDishes/$', lookups.LookupDessertDishes.as_view(), name='lookup_dessertDishes'),
    re_path(r'^lookup_menu_list/$', lookups.LookupMenuList.as_view(), name='lookup_menu_list'),

# start
    re_path(r'^$', views.Main.as_view()),
]
