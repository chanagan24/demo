from django import forms
from demoWeb.extends import formext
from crispy_forms.layout import Layout, Div
from crispy_forms.helper import FormHelper
from dal import autocomplete
from dal import forward

from . import models


class reportFindForm(formext.BaseEditForm):
    main = forms.ModelChoiceField(label='Menu',
                                    queryset=models.menu_list.objects.none(),
                                    widget=autocomplete.ModelSelect2(url='bMisc:lookup_menu_list',
                                                                     attrs={'data-placeholder': 'Select Menu...'},
                                                                     forward=[forward.Const('1', 'stat')]))

    rtype = forms.ChoiceField(label='รูปแบบ',
                              choices=[],
                              widget=autocomplete.ListSelect2(url='bMisc:lookup_report_type',
                                                              attrs={'data-placeholder': 'Type ...', },
                                                              forward=[forward.Const('1', 'test')]))

    helper = FormHelper()
    helper.form_tag = False
    helper.include_media = True
    helper.layot = Layout(
        Div(
            Div('main', css_class="col-md-3"),
            Div('rtype', css_class="col-md-3"),
            css_class='row',
        ),
    )


class MenuList(formext.BaseEditForm):

    main = forms.ChoiceField(label='Main Dishes',
                                    widget=autocomplete.ListSelect2(url='bMisc:lookup_mainDishes',
                                                                     attrs={'data-placeholder': 'Select Main...'},
                                                                     forward=[forward.Const(1, 'stat')]))

    dessert = forms.ChoiceField(label='Dessert Dishes',
                                    widget=autocomplete.ListSelect2(url='bMisc:lookup_dessertDishes',
                                                                     attrs={'data-placeholder': 'Select Dessert ...'},
                                                                     forward=[forward.Field('main', '1')]))

    helper = FormHelper()
    helper.form_class = 'form-horizontal'
    helper.label_class = 'col-sm-2'
    helper.field_class = 'col-sm-6'
    helper.form_tag = False
    helper.layout = Layout(
        Div(
            Div('main'),
            Div('dessert'),
            css_class='form-group'),
    )
