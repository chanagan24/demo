from django.db import models

# Create your models here.
class menu_list(models.Model):
    id = models.AutoField(primary_key=True, verbose_name='ID')
    name = models.CharField(max_length=225, blank=True, null=True, verbose_name='Name')

    class Meta:
        managed = False
        db_table = 'menu_list'
        ordering = ['id']
        verbose_name = "Menu Lists"

    def __str__(self):
        return '{}-{}'.format(self.id, self.name)
